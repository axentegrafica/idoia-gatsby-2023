import * as React from "react";
import "./Header.css";
import { Link } from "gatsby";
import MobileMenu from "./MobileMenu";

export function Header() {
  return (
    <header>
      <div className="header-menu">
        <Link className="simboloservizos" to="/">
          <img src="/images/simboloido.svg" />
        </Link>
        <nav className="nav-izda">
          <ul className="nav-ul">
            <li>
              <Link to="/ilustracion">Ilustración</Link>
            </li>
            <li>
              <Link to="/tipografia">Tipografía</Link>
            </li>
          </ul>
        </nav>
        <nav className="nav-dcha">
          <ul className="nav-ul">
            <li>
              <Link to="/desenho">Deseño gráfico</Link>
            </li>
            <li>
              <Link to="/editorial">Editorial</Link>
            </li>
          </ul>
        </nav>
      </div>
      <MobileMenu />
    </header>
  );
}
