import * as React from "react";
import "./FontPreview.css";
import { useState } from "react";
import parse from "html-react-parser";

export function FontPreview(props) {
  const INITIAL_FONT_SIZE = 60;

  const [text, setText] = useState('ABCDEFG!"12345!·*^¨-...');
  const [size, setSize] = useState(INITIAL_FONT_SIZE);
  const lineHeight = size + 10;

  const handleClickLoremIpsum = () => setText("Lorem ipsum dolor sit amet...");
  const handleClickResetText = () => setText('ABCDEFG!"12345!·*^¨-...');
  const handleRangeChange = (e) => setSize(Number(e.target.value));
  const handleChangeText = (e) => setText(e.target.value);

  const description = props.description;
  const image0 = props.image0;
  const image1 = props.image1;
  const image2 = props.image2;
  const image3 = props.image3;
  const image4 = props.image4;
  const abcfonte = props.abcfonte;
  const altImage0 = props.altImage0;

  return (
    <>
      <section className={`tipografia ${props.className}`}>
        <h2>{props.name}</h2>
        <div className="size">
          <label htmlFor="size">
            Desliza o punto para axustar o corpo de letra:
          </label>
          <input
            value={size}
            onChange={handleRangeChange}
            type="range"
            id="size"
            className="buttonrangesize"
            name="size"
            min="20"
            max="120"
            step="1"
          />
        </div>
        <label htmlFor="vertipo">
          Proba a escribir coa tipografía Partitura1941:
        </label>
        <textarea
          style={{
            fontSize: `${size}px`,
            lineHeight: `${lineHeight}px`,
          }}
          id="vistatipo"
          name="vistatipo"
          value={text}
          onChange={handleChangeText}
        ></textarea>
        <button className="botonestipo" onClick={handleClickLoremIpsum}>
          Simular Lorem Ipsum
        </button>
        <button className="botonestipo" onClick={handleClickResetText}>
          Restaurar texto orixinal
        </button>
        <div id="especimen">
          <div className="presentacionfont">
            <img
              className="imaxefonte1txt"
              src={`/images/tipografia/${props.image0}`}
              alt={altImage0}
            />
            <div className="coltexto">{parse(description)}</div>
          </div>
          <div className="cascada">
            <p className={`abcfonte ${props.className}`}>{abcfonte}</p>
          </div>
        </div>
        <figure id="mockambastipos">
          <img
            className="mockupsfontes"
            src={`/images/tipografia/${props.image1}`}
            alt="Exemplo 1 de mostra de como quedaría a tipografía en distintos soportes e acabados."
          />
          <img
            className="mockupsfontes"
            src={`/images/tipografia/${props.image2}`}
            alt="Exemplo 2 de mostra de como quedaría a tipografía en distintos soportes e acabados."
          />
          <img
            className="mockupsfontes"
            src={`/images/tipografia/${props.image3}`}
            alt="Exemplo 3 de mostra de como quedaría a tipografía en distintos soportes e acabados."
          />
          <img
            className="mockupsfontes"
            src={`/images/tipografia/${props.image4}`}
            alt="Exemplo 4 de mostra de como quedaría a tipografía en distintos soportes e acabados."
          />
        </figure>
      </section>
    </>
  );
}
