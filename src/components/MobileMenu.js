import * as React from "react";
import "./MobileMenu.css";
import { Link } from "gatsby";

const MobileMenu = () => {
  const [open, setOpen] = React.useState(false);

  const handleButtonClick = () => {
    setOpen(!open);
  };
  const closeMenu = () => {
    setOpen(false);
  };

  return (
    <>
      <div className="nav">
        <button className="btn-nav" onClick={handleButtonClick}>
          <span className="icon-bar top"></span>
          <span className="icon-bar middle"></span>
          <span className="icon-bar bottom"></span>
        </button>
      </div>
      <div className={`nav-content ${open ? "showNav" : "hideNav hidden"}`}>
        <ul className="nav-list">
          <li className="nav-item">
            <Link to="/" className="item-anchor" onClick={closeMenu}>
              Servizos
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/tipografia" className="item-anchor" onClick={closeMenu}>
              Tipografía
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/ilustracion" className="item-anchor" onClick={closeMenu}>
              Ilustración
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/desenho" className="item-anchor" onClick={closeMenu}>
              Deseño Gráfico
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/editorial" className="item-anchor" onClick={closeMenu}>
              Editorial
            </Link>
          </li>
        </ul>
        <div className="line-betwen"></div>
      </div>
    </>
  );
};

export default MobileMenu;
