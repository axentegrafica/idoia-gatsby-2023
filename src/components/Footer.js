import * as React from "react";
import "./footer.css";
import "../../static/fontello/css/fontello.css";
import { Link } from "gatsby";

export function Footer() {
  return (
    <footer>
      <section id="bios">
        <div className="containercenter">
          <div id="biosleft">
            <div id="biocvs">
              <p className="cvs">
                <a href="/dwd/CVIdoiadeLuxan.pdf">
                  <i className="demo-icon icon-attach"></i> CV
                </a>
              </p>
              <p className="cvs">
                <a href="/dwd/IdoiadeLuxanPortfolio.pdf">
                  <i className="demo-icon icon-attach"></i> BOOK
                </a>
              </p>
              <div id="biomarcofoto">
                <img
                  className="biofoto"
                  src="/images/fotocv1.webp"
                  alt="Idoia de Luxán"
                />
              </div>
            </div>
            <div id="bionome">
              <p className="nome">IDOIA DE LUXÁN</p>
              <p className="hashtag">
                <a
                  href="https://www.instagram.com/idoiadluxan/"
                  target="_blank"
                  rel="noreferrer"
                >
                  @idoiadluxan
                </a>
              </p>
            </div>
          </div>
          <div id="biosright">
            <blockquote className="lema">17 anos ideando!</blockquote>
            <nav className="socialmedia">
              <ul>
                <li>
                  <a
                    href="https://es.linkedin.com/in/idoia-de-lux%C3%A1n-6388179"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="demo-icon icon-linkedin"></i>
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.instagram.com/idoiadluxan/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="demo-icon icon-instagram"></i>
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.arteinformado.com/guia/f/idoia-de-luxan-vazquez-idoia-de-luxan-220237"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="demo-icon icon-ainf"></i>
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.myfonts.com/collections/partitura1941-font-idoia-de-luxan"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="demo-icon icon-myf"></i>
                  </a>
                </li>
                <li>
                  <a
                    href="mailto:info@idoiadeluxan.com"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="demo-icon icon-mail"></i>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </section>
      <div>
        <nav>
          <ul className="footer-como">
            <li>
              <Link to="/faq">Ética</Link>
            </li>
            <li>
              <Link to="/faq">Métodos</Link>
            </li>
          </ul>
        </nav>
        <nav>
          <ul className="footer-menu">
            <li>
              <Link to="/avisol">Aviso Legal</Link>
            </li>
            <li>
              <Link to="/avisol">Politica de privadidade</Link>
            </li>
          </ul>
        </nav>
      </div>
    </footer>
  );
}
