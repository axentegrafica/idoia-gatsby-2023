import * as React from "react";
import { Header } from "../components/Header";
import { Footer } from "../components/Footer";
import "./tipografia.css";
import { FontPreview } from "/src/components/FontPreview";
import { SEO } from "../components/seo";

const pageStyles = {
  color: "#232129",
  padding: 96,
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
};
const headingStyles = {
  marginTop: 0,
  marginBottom: 64,
  maxWidth: 320,
};

const TipografiaPage = () => {
  const fonts = [
    {
      name: "Partitura1941",
      className: "partitura",
      description:
        "<p>Tipografía caligráfica que se deseñou inspirándose nas notas manuscritas dun caderno de partituras e notación musical familiar de 1941. O meu gusto pola música, que vén de familia, fixo deste proxecto persoal algo moi especial para min coma tipografista. Apta para títulos ou simulacións de notacións ou escritura da época. Non está construída para ser empregada en textos longos, salvo casos moi específicos. Funciona moi ben en aplicacións para bodas, eventos gourmet, temáticas de terror ou misterio, graffiti, metal, folk, programación musical en xeral, tattoo ou calquera aplicación de publicidade na que encaixe estéticamente.</p><ul><li>Familia: 1</li><li>Estilos: 1</li><li>Idiomas: 21</li></ul> ",
      abcfonte:
        "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyz 0123456789.,:;´ç¨Ç`+^*!%&/()=¿?",
      image0: "imaxefonte.webp",
      altImage0: "mostra tipografía Partitura1941",
      image1: "Partitura1941spec1.webp",
      image2: "Partitura1941spec2.webp",
      image3: "Partitura1941spec3.webp",
      image4: "Partitura1941spec4.webp",
    },
    {
      name: "Kallaikos Revve",
      className: "kallaikos",
      description:
        "<p>Recreación estimativa da letra indíxena galaica. Baseada na auténtica escritura empregada polas poboacións céltigas de Galiza. Inspirada na epigrafía que aparece, entre outros, nos Guerreiros Galaicos de Santa Comba e San Paio de Meixedo (Portugal), adaptada, dibuxada e construída coma tipografía por Idoia de Luxán, partindo da documentación aportada por Paco Boluda. Foi empregada no propio libro de Paco Boluda 'KALLAIKOS. Unha viaxe á Galiza céltica.' Fíxena a xeito de agasallo a Paco Boluda, en agradecemento polo tempo que adica a investigar e compartir a nosa historia nativa. Apta para títulos, non apta para textos longos nin para usos descontextualizados.</p><ul><li>Familia:  1.</li><li>Estilos: 1.</li><li>Idiomas: compatibilidade completa. </li></ul>",
      abcfonte:
        "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyz 0123456789.,:;´ç¨Ç`+^*!%&/()=¿?",
      image0: "imaxefonte3.webp",
      altImage0: "mostra tipografía Kallaikos Revve",
      image1: "kallaikosspc3.webp",
      image2: "kallaikosspc4.webp",
      image3: "kallaikosspc5.webp",
      image4: "kallaikosspc6.webp",
    },
  ];
  return (
    <>
      <Header />
      <main>
        <figure id="portadatipo"></figure>

        <section className="services">
          <h1>TIPOGRAFÍA</h1>
          <section className="txtilust">
            <div>
              <p>
                Desenvolve proxextos tipográficos de distinto tipo. Deseñando,
                caracter por caracter, os conxuntos de glifos necesarios para
                ser empregados na maior cantidade de idiomas posible, polas
                distintas aplicacións informáticas de deseño e maquetación de
                textos, nas codificacións de caracteres máis empregadas e cos
                aspectos técnicos necesarios para a súa correcta lexibilidade.
                Por exemplo, para a tipografía caligráfica Partitura1941 foi
                necesario deseñar 374 caracteres e as súas correspondentes
                ligaduras. Aposta polas Fontes de Tipo Aberto, como OTF, Open
                Type Fonts e as licencias de uso SIL OFL, nos casos de
                distribución gratuíta, como Kallaikosrevve.
              </p>
              <p>
                Estes deseños tipográficos, poden ser mercados ou descargados,
                instalados nas fontes do sistema e empregados desde calquera
                dispositivo por calquera persoa.
              </p>
              <p>
                Tamén é posible o encargo de desenvolvemento de tipografías de
                pau seco, con serifa, manuscritas, caligráficas ou decorativas.
                Do mesmo xeito, tamén se realizan traballos de lettering
                específicos para eventos, proxectos ou o que xurda. Pódense
                consultar as condicións para estes casos en “Métodos” ou a
                través do correo ou o teléfono.
              </p>
            </div>
          </section>
          {fonts.map((font) => (
            <FontPreview
              name={font.name}
              className={font.className}
              key={font.className}
              description={font.description}
              image0={font.image0}
              altImage0={font.altImage0}
              abcfonte={font.abcfonte}
              image1={font.image1}
              image2={font.image2}
              image3={font.image3}
              image4={font.image4}
            />
          ))}
        </section>
        <section className="txtilust">
          <div>
            <h2>Curiosidades e rumores...</h2>
            <p class="txtcuriosidades">
              Aprendeu a escribir soa antes da gardería. Trazando as letras do
              revés, como boa ambidextra. Xa no colexio desfrutaba maquetando os
              traballos de clase a man e con collage. Customizaba o letreado
              para unha boa xerarquía, dándolle un toque para que tivese un
              estilo fresco.
            </p>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default TipografiaPage;

export const Head = () => <SEO title="Tipografía" />;
