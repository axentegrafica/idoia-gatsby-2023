import * as React from "react";
import { Header } from "../components/Header";
import { Footer } from "../components/Footer";
import "./faq.css";
import { SEO } from "../components/seo";

const FaqPage = () => {
  return (
    <>
      <Header />
      <main>
        <figure id="portadafaq"></figure>
        <h1 id="etica">ÉTICA</h1>
        <section class="etica">
          <details>
            <summary>Economía circular aplicada á empresa.</summary>
            <p class="metodos">
              Estás a escoller servizos profesionais comprometidos co medio
              ambiente. Aplicando a economía circular á empresa e promovendo a
              economía local, debuxando un futuro sostible para todas as
              especies.
            </p>
          </details>
          <details>
            <summary>
              Xestión ambiental e enerxética. Marketing ecolóxico.
            </summary>
            <p class="metodos">
              A xestión ambiental e enerxética, así como as técnicas de
              marketing ecolóxico, son coidadosamente deseñadas para cursar o
              menor dano posible ao planeta, deixando a menor pegada e
              favorecendo a existenza de recursos sostibles.
            </p>
          </details>
          <details>
            <summary>Proteción e defensa dos dereitos humáns.</summary>
            <p class="metodos">
              Esta empresa, rexentada por unha muller, cumple un compromiso
              social e contribúe coa cooperación e desenvolvemento. Escollendo
              con sumo coidado os proveidores de recursos e perseguindo a
              proteción e defensa dos dereitos humáns. Ademáis, préstase
              especial coidado con respecto á proteción dos dereitos da
              infancia, en todo o planeta.
            </p>
          </details>
          <details>
            <summary>
              Acesible e amigable coas persoas, consciente da diversidade
              social.{" "}
            </summary>
            <p class="metodos">
              Éste é un espazo de servizos aberto a todo tipo de persoas, que
              busca a seguridade e comodidade da clientela, así como a calidade
              da atención: LGTBIQA+, racializadas, migrantes, supervivintes á
              violencia de xénero, con funcionalidade diversa, neurodiverxentes,
              masculinidades sensibles ou personalidades diversas, por mencionar
              exemplos. Se atopas algún aspecto a mellorar, non dubides en
              comunicalo. Esta empresa fórxase a través da aprendizaxe ao longo
              da vida e desde o respeto e a experiencia. Este é un espazo que
              busca ser acesible para todas as persoas, participando nunha
              transformación social no local e no global.
            </p>
          </details>
          <details>
            <summary>Espazo libre de violencia. </summary>
            <p class="metodos">
              Nesta web, na comunicación e no transcurso dos traballos e
              encargos, non haberá cabida para ningún tipo ou forma de violencia
              machista, racista, capacitista, xenófoba, homófoba, tránsfoba,
              aporófoba, fascista nin de ningún tipo.
            </p>
          </details>
        </section>
        <h1 id="metodos">MÉTODOS</h1>
        <section class="metodos-section">
          <details>
            <summary>Toma de contacto</summary>
            <p class="metodos">
              Coñecémonos, cóntasme o que precisas e acoto para poder imaxinar
              como vou cubrir as túas necesidades gráficas cun resultado óptimo.
            </p>
          </details>
          <details>
            <summary>Orzamento</summary>
            <p class="metodos">
              En base á información recibida, enviareite un orzamento á medida,
              escalado ou unha iguala, si procede.
            </p>
            <ul class="opcions-orzamento">
              <li class="liopcions">
                <h3 class="orzamento">Á medida</h3>
                <p class="metodos">
                  Orzamento xusto para ambas partes, que cubra as miñas
                  necesidades e sexa asequible e asumible para ti, coa calidade
                  de traballo axeitado.
                </p>
              </li>
              <li class="liopcions">
                <h3 class="orzamento">Escalado</h3>
                <p class="metodos">
                  Cando non tés claro como vas acotar o traballo e o seu prezo,
                  fágoche un escalado para que poidas escoller o que se axuste
                  ao que queres.
                </p>
              </li>
              <li class="liopcions">
                <h3 class="orzamento">Iguala</h3>
                <p class="metodos">
                  Si o que queres é optar os meus servizos remotos de forma
                  continuada no tempo, unha iguala é unha boa opción. Nela,
                  dareite opcións de tarifas planas para que escollas a mellor
                  para ti.
                </p>
              </li>
            </ul>
            <h3 class="orzamento">Condicións</h3>
            <p class="metodos">
              No caso da iguala, irán incluídas no contrato de servizos. No caso
              dos outros tipos de orzamento, acotaráse o que inclúen no apartado
              "observacións" do orzamento.
            </p>
          </details>
          <details>
            <summary>Desenvolvemento do proxecto</summary>
            <p class="metodos">
              Elaborarei o proxecto encargado acorde ás indicacións e orzamento,
              en prazo e forma. De haber calquera cambio imprevisto, por
              calquera das partes, pactaremos a mellor adaptación para todas as
              partes.
            </p>
          </details>
          <details>
            <summary>Forma de pago</summary>
            <p class="metodos">
              Por transferencia, á conta que se especifica na factura
              correspondente e no número de pagos pactado. Para proxectos
              grandes, pódese solicitar un pago a prazos sen intereses.
            </p>
          </details>
          <details>
            <summary>Resultado</summary>
            <p class="metodos">
              Celebrarei que o resultado supere as túas espectativas e que a
              experiencia fose óptima.
            </p>
          </details>
          <details>
            <summary>Agradecemento</summary>
            <p class="metodos">
              Podemos tecer rede de apoio, dándonos recomendacións e
              agradecemento mutuo. Esto axuda moito a acadar audiencia e
              fidelización. Si precisas novos servizos ou coñeces alguén que os
              busque, estarei encantada de atendervos. O mesmo fago eu sempre
              que podo, coa clientela e provedores de servizos alleos. Do mesmo
              xeito, podemos retroalimentarnos a través de reseñas ou
              recomendacións en base ao resultado do proxecto.
            </p>
          </details>
          <details>
            <summary>Destinatarios</summary>
            <p class="metodos">
              Cooperativas, iniciativas e entidades de economía social, PYMES,
              profesionais autónomas, imprentas, editoriais, ONGs, asociacións,
              grupos de participación social, centros, calquera persoa,
              proxecto, empresa, axencia, organismo público ou profesional de
              calquera punto do globo...
            </p>
          </details>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default FaqPage;

export const Head = () => <SEO title="FAQ" />;
