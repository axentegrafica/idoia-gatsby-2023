import * as React from "react";
import { Header } from "../components/Header";
import { Footer } from "../components/Footer";
import "./avisol.css";
import { SEO } from "../components/seo";

const avisolPage = () => {
  return (
    <>
      <Header />
      <main>
        <figure id="portadaavisol"></figure>
        <h1 id="avisol">AVISO LEGAL</h1>
        <section class="avisol">
          <details>
            <summary>1. Información Xeral</summary>
            <p class="avisol">
              En cumprimiento da Lei 34/2002, de 11 de xullo, de Servizos da
              Sociedade da Información e do Comercio Electrónico, infórmase que
              o presente sitio web é propiedade de Idoia de Luxán Vázquez,
              profesional freelance con domicilio en Lugar de Vilariño 12, CP
              32811 Vilariño, Ramirás, e con NIF 44485697V.
            </p>
          </details>
          <details>
            <summary>2. Propiedade Intelectual</summary>
            <p class="avisol">
              Todos os contidos deste sitio web, incluídos textos, imaxes,
              gráficos, deseños e calquera outro material, son propiedade de
              Idoia de Luxán Vázquez ou de terceiros que autorizaron o seu uso.
              Queda prohibida a reprodución total ou parcial dos contidos, sen o
              consentimento expreso de Idoia de Luxán Vázquez. Así mesmo, queda
              terminantemente prohibida a elaboración de obra derivada,
              modificacións ou inclusión de calquera obra contida nesta web, en
              ningún repositorio de imaxes ou motores de IA, plaxio, uso non
              autorizado, roubo ou calquera forma de uso indebido. Empregarase a
              lexislación vixente en materia de propiedade intelectual, dereitos
              de autor e dereitos patrimoniáis.
            </p>
          </details>
          <details>
            <summary>3. Uso do Sitio Web</summary>
            <p class="avisol">
              O usuario comprométese a utilizar o sitio web de conformidade coa
              lei, a moral e as boas costumes, así como a non realizar
              actividades que poidan prexudicar a Idoia de Luxán Vázquez ou a
              terceiros.
            </p>
          </details>
          <details>
            <summary>4. Enlaces a Terceiros</summary>
            <p class="avisol">
              O sitio web pode conter enlaces a outras páxinas web. Idoia de
              Luxán Vázquez non se fai responsable dos contidos de ditos sitios
              nin das políticas de privacidade que poidan aplicar.
            </p>
          </details>
          <details>
            <summary>5. Protección de Datos</summary>
            <p class="avisol">
              Idoia de Luxán Vázquez comprométese a protexer a privacidade dos
              seus usuarios. A información persoal que se recolla a través deste
              sitio web será tratada de acordo coa lexislación vixente en
              materia de protección de datos. Para máis información, consulta a
              nosa Política de Privacidade.
            </p>
          </details>
          <details>
            <summary>6. Modificacións</summary>
            <p class="avisol">
              Idoia de Luxán Vázquez resérvase o dereito a modificar este aviso
              legal en calquera momento. É responsabilidade do usuario revisar
              periódicamente este aviso para estar ao tanto das modificacións
            </p>
          </details>
          <details>
            <summary>7. Lexislación Aplicable</summary>
            <p class="avisol">
              Este aviso legal réxese pola lexislación española. Para calquera
              controversia que puidese derivarse da utilización do sitio web, as
              partes sométense á xurisdición dos xulgados e tribunais de
              Ramirás.
            </p>
          </details>
        </section>
        <h1 id="avisol">Política de Privacidade</h1>
        <section class="avisol">
          <details>
            <summary>1. Información Xeral</summary>
            <p class="avisol">
              En Idoia de Luxán Vázquez, comprometémonos a protexer a
              privacidade dos nosos usuarios. Esta Política de Privacidade
              establece como recopilamos, utilizamos e protexemos a información
              persoal que nos proporcionas a través do noso sitio web
              https://www.idoiadeluxan.com.
            </p>
          </details>
          <details>
            <summary>2. Datos Recollidos</summary>
            <p class="avisol">
              Esta web non recopila datos de caracter persoal. A través do uso
              do correo electrónico, podemos recopilar os seguintes tipos de
              información persoal:
            </p>
            <ul>
              <li class="privacidade">Nome</li>
            </ul>
            <ul>
              <li class="privacidade">Correo electrónico</li>
            </ul>
            <ul>
              <li class="privacidade">Información de contacto.</li>
            </ul>
            <ul>
              <li class="privacidade">
                Calquera outro dato que decidas proporcionarnos a través do
                correo electrónico ou subscricións a redes sociais.
              </li>
            </ul>
          </details>
          <details>
            <summary>3. Uso da Información</summary>
            <p class="avisol">
              A información que de poida recopilar a través do correo
              electrónico ou redes sociais utilízase para:
            </p>
            <ul>
              <li class="privacidade">
                Responder ás túas consultas e comentarios.
              </li>
            </ul>
            <ul>
              <li class="privacidade">
                Enviarcho información sobre os nosos servizos e novidades.
              </li>
            </ul>
          </details>
          <details>
            <summary>4. Cookies</summary>
            <p class="avisol">
              O noso sitio web pode utilizar cookies para mellorar a experiencia
              do usuario. As cookies son pequenos arquivos de texto que se
              almacenan no teu dispositivo. Podes configurar o teu navegador
              para que non acepte cookies, pero isto podería afectar a
              funcionalidade do noso sitio.
            </p>
          </details>
          <details>
            <summary>5. Protección de Datos</summary>
            <p class="avisol">
              Tomamos medidas razoables para protexer a información persoal que
              recopilamos. Non obstante, ten en conta que ningunha transmisión
              de datos a través de Internet é completamente segura.
            </p>
          </details>
          <details>
            <summary>6. Dereitos do Usuario</summary>
            <p class="avisol">
              Tes dereito a acceder, rectificar e eliminar os teus datos
              persoais. Se desexas exercer estes dereitos, por favor contáctanos
              a través de info@idoiadeluxan.com.
            </p>
          </details>
          <details>
            <summary>7. Cambios na Política de Privacidade</summary>
            <p class="avisol">
              Resérvamonos o dereito a modificar esta Política de Privacidade en
              calquera momento. Recoméndoche que revises esta páxina
              periódicamente para estar ao tanto de calquera cambio.
            </p>
          </details>
          <details>
            <summary>8. Lexislación Aplicable</summary>
            <p class="avisol">
              Esta Política de Privacidade réxese pola lexislación española.
              Para calquera controversia que puidese derivarse da utilización do
              sitio web, as partes sométense á xurisdición dos xulgados e
              tribunais de Ourense.
            </p>
          </details>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default avisolPage;

export const Head = () => <SEO title="Aviso Legal" />;
