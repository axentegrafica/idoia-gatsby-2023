import * as React from "react";
import { Header } from "../components/Header";
import { Footer } from "../components/Footer";
import { Fade } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import "./editorial.css";
import { SEO } from "../components/seo";

const slideImages1 = [
  {
    url: "/images/editorial/escritorioeditcat.webp",
    url_tablet: "/images/editorial/tableteditcat.webp",
    url_mobile: "/images/editorial/mobileeditcat.webp",
    caption: "Catálogos",
    alt: "Catálogos",
  },
  {
    url: "/images/editorial/escritorioeditcat2.webp",
    url_tablet: "/images/editorial/tableteditcat2.webp",
    url_mobile: "/images/editorial/mobileeditcat2.webp",
    caption: "Guías",
    alt: "Guías",
  },
  {
    url: "/images/editorial/escritorioeditautoed1.webp",
    url_tablet: "/images/editorial/tableteditautoed1.webp",
    url_mobile: "/images/editorial/mobileeditautoed1.webp",
    caption: "Auto edición",
    alt: "Auto edición",
  },
  {
    url: "/images/editorial/escritorioeditbd.webp",
    url_tablet: "/images/editorial/tableteditbd.webp",
    url_mobile: "/images/editorial/mobileeditbd.webp",
    caption: "Banda deseñada",
    alt: "Banda deseñada",
  },
  {
    url: "/images/editorial/escritorioeditbd2.webp",
    url_tablet: "/images/editorial/tableteditbd2.webp",
    url_mobile: "/images/editorial/mobileeditbd2.webp",
    caption: "BD",
    alt: "BD",
  },
  {
    url: "/images/editorial/escritorioeditportadas.webp",
    url_tablet: "/images/editorial/tableteditportadas.webp",
    url_mobile: "/images/editorial/mobileeditportadas.webp",
    caption: "Deseño de portadas",
    alt: "Deseño de portadas",
  },
];

const slideImages2 = [
  {
    url: "/images/editorial/escritorioeditlibesp.webp",
    url_tablet: "/images/editorial/tableteditlibesp.webp",
    url_mobile: "/images/editorial/mobileeditlibesp.webp",
    caption: "Edicións especiais",
    alt: "Edicións especiais",
  },
  {
    url: "/images/editorial/escritorioeditautoed2.webp",
    url_tablet: "/images/editorial/tableteditautoed2.webp",
    url_mobile: "/images/editorial/mobileeditautoed2.webp",
    caption: "Investigación",
    alt: "Investigación",
  },
  {
    url: "/images/editorial/escritorioeditautoed3.webp",
    url_tablet: "/images/editorial/tableteditautoed3.webp",
    url_mobile: "/images/editorial/mobileeditautoed3.webp",
    caption: "Autoedición",
    alt: "Autoedición",
  },
  {
    url: "/images/editorial/escritorioeditcatxap.webp",
    url_tablet: "/images/editorial/tableteditcatxap.webp",
    url_mobile: "/images/editorial/mobileeditcatxap.webp",
    caption: "Catálogos (xaponés)",
    alt: "Catálogos (xaponés)",
  },
  {
    url: "/images/editorial/escritorioeditxuv.webp",
    url_tablet: "/images/editorial/tableteditxuv.webp",
    url_mobile: "/images/editorial/mobileeditxuv.webp",
    caption: "Xuvenil",
    alt: "Xuvenil",
  },
];

const indicators1 = (index) => (
  <div className="indicator">{slideImages1[index].caption}</div>
);
const indicators2 = (index) => (
  <div className="indicator">{slideImages2[index].caption}</div>
);

const editorialPage = () => {
  return (
    <>
      <Header />
      <main>
        <figure id="portadaeditorial"></figure>

        <section className="editorial-section1">
          <h1>Editorial</h1>
          <div className="slide-container">
            <Fade indicators={indicators1}>
              {slideImages1.map((fadeImage, index) => (
                <div key={index}>
                  <img
                    className="desktop"
                    style={{ height: "100%" }}
                    src={fadeImage.url}
                    alt={fadeImage.caption}
                  />
                  <img
                    className="tablet"
                    style={{ height: "100%" }}
                    src={fadeImage.url_tablet}
                    alt={fadeImage.caption_tablet}
                  />
                  <img
                    className="mobile"
                    style={{ height: "100%" }}
                    src={fadeImage.url_mobile}
                    alt={fadeImage.caption_mobile}
                  />
                </div>
              ))}
            </Fade>
          </div>
        </section>

        <section className="txtdes">
          <div>
            <p>
              Maqueta e realiza deseño editorial, en diferentes idiomas, para
              proxectos como: catálogos de viaxes, catálogos de arte, folletos
              promocionais, revistas, libros, coleccións de libros, banda
              deseñada, fanzines, teses doutorais, manuais didácticos, materiais
              educativos, libros de texto, unidades didácticas, cadernos,
              proxectos de autoedición. Ten maquetado proxectos para todo o
              globo, incluíndo Malasia ou Xapón. Deu forma a proxectos de
              autoedición moi esperados e a outros sorprendentes e nostálxicos.
              Desfrutou dun aceso privilexiado ás investigacións relativas a
              estes traballos, e a outros como a tese doutoral de Ángel
              Domínguez López “Estudio documental del arte de la platería en las
              parroquias de la diócesis de Ourense. Del manierismo al barroco”.
              Esta última, resultou en dous tomos de 672 e 404 páxinas cheas de
              fotografías sobre pezas de pratería.{" "}
            </p>
            <p>
              Viaxa a través das páxinas, coidando con esmero as imaxes, a
              lexibilidade, a composición, o estilo e o acabado. O libro final é
              o seu destiño favorito.
            </p>
          </div>
        </section>
        <section className="editorial-section2">
          <div className="slide-container">
            <Fade indicators={indicators2}>
              {slideImages2.map((fadeImage, index) => (
                <div key={index}>
                  <img
                    className="desktop"
                    style={{ height: "100%" }}
                    src={fadeImage.url}
                    alt={fadeImage.caption}
                  />
                  <img
                    className="tablet"
                    style={{ height: "100%" }}
                    src={fadeImage.url_tablet}
                    alt={fadeImage.caption_tablet}
                  />
                  <img
                    className="mobile"
                    style={{ height: "100%" }}
                    src={fadeImage.url_mobile}
                    alt={fadeImage.caption_mobile}
                  />
                </div>
              ))}
            </Fade>
          </div>
        </section>
        <section className="txtilust">
          <div>
            <h2>Curiosidades e rumores...</h2>
            <p class="txtcuriosidades">
              De cativa, imaxinaba, con moita curiosidade, cómo sería traballar
              deseñando ou ilustrando para editoriais especializadas en banda
              deseñada, como as francesas... ou de libros infantís. Como
              estudante de artes e deseño gráfico, soñaba con adicarse ao deseño
              editorial. Ao estilo de Castelao, Luis Seoane, Katsushika Hokusai,
              Daniel Gil, Jan Tschichold, Charles Rennie Mackintosh, Aubrey
              Beardsley ou William Morris. Foi consciente, nese momento, da
              necesidade inminente de referentes no sector, que non fosen homes
              brancos. Cerciorouse do seu vínculo e influencia estética do mundo
              prerromán e medieval, perdéndose, durante horas, en arquivos
              dixitais de manuscritos iluminados.
            </p>
            <p class="txtcuriosidades">
              O primeiro manual que maquetou, estaba en neerlandés. Fórmase en
              biblioteconomía e docencia, abrindo novos posibles portais de
              inmersión no mundo profesional da edición de libros e do fomento
              da lectura.
            </p>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default editorialPage;

export const Head = () => <SEO title="Deseño Editorial" />;
