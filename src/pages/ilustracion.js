import * as React from "react";
import { Header } from "../components/Header";
import { Footer } from "../components/Footer";
import { Fade } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import "./ilustracion.css";
import { SEO } from "../components/seo";

const slideImages1 = [
  {
    url: "/images/ilustracion/ilusthcapastwomen.webp",
    url_tablet: "/images/ilustracion/tabletilustracionpastwomen.webp",
    url_mobile: "/images/ilustracion/mobileilustracionpastwomen.webp",
    caption: "Histórica",
    alt: "Histórica",
  },
  {
    url: "/images/ilustracion/escritorioilustinfantil.webp",
    url_tablet: "/images/ilustracion/tabletilustinfantil.webp",
    url_mobile: "/images/ilustracion/mobileilustinfantil.webp",
    caption: "Infantil",
    alt: "Ilustración infantil",
  },
  {
    url: "/images/ilustracion/ilusthumorgrafico.webp",
    url_tablet: "/images/ilustracion/tabletilusthumorgrafico.webp",
    url_mobile: "/images/ilustracion/mobileilusthumorgrafico.webp",
    caption: "Humor gráfico",
    alt: "Humor gráfico",
  },
  {
    url: "/images/ilustracion/ilustbd.webp",
    url_tablet: "/images/ilustracion/tabletilustbd.webp",
    url_mobile: "/images/ilustracion/mobileilustbd.webp",
    caption: "Cómic",
    alt: "Cómic, BD ou banda deseñada",
  },
  {
    url: "/images/ilustracion/ilustxogos.webp",
    url_tablet: "/images/ilustracion/tabletilustxogos.webp",
    url_mobile: "/images/ilustracion/mobileilustxogos.webp",
    caption: "Xogos",
    alt: "Xogos de mesa e RPG",
  },
  {
    url: "/images/ilustracion/ilustlivepainting.webp",
    url_tablet: "/images/ilustracion/ilustlivepaintingtablet.webp",
    url_mobile: "/images/ilustracion/ilustlivepaintingmobile.webp",
    caption: "Live Painting",
    alt: "Live Painting",
  },
  {
    url: "/images/ilustracion/ilustpubli.webp",
    url_tablet: "/images/ilustracion/tabletiluspubli.webp",
    url_mobile: "/images/ilustracion/mobileilustpubli.webp",
    caption: "Publicitaria",
    alt: "Publicitaria",
  },
];

const slideImages2 = [
  {
    url: "/images/ilustracion/ilustlivepainting2.webp",
    url_tablet: "/images/ilustracion/ilustlivepaintingtablet2.webp",
    url_mobile: "/images/ilustracion/ilustlivepaintingmobile2.webp",
    caption: "Live painting",
    alt: "Live painting",
  },
  {
    url: "/images/ilustracion/ilustpubliab.webp",
    url_tablet: "/images/ilustracion/tabletilustpubliab.webp",
    url_mobile: "/images/ilustracion/mobileilustpubliab.webp",
    caption: "Publicitaria",
    alt: "Publicitaria",
  },
  {
    url: "/images/ilustracion/ilustbdab.webp",
    url_tablet: "/images/ilustracion/tabletilustbdab.webp",
    url_mobile: "/images/ilustracion/mobileilustbdab.webp",
    caption: "BD",
    alt: "BD",
  },
  {
    url: "/images/ilustracion/ilushcaab.webp",
    url_tablet: "/images/ilustracion/tabletilusthcaab.webp",
    url_mobile: "/images/ilustracion/mobileilusthcaab.webp",
    caption: "Histórica",
    alt: "Histórica",
  },
  {
    url: "/images/ilustracion/ilushca3ab.webp",
    url_tablet: "/images/ilustracion/tabletilusthca3ab.webp",
    url_mobile: "/images/ilustracion/mobileilusthca3ab.webp",
    caption: "Arqueolóxica",
    alt: "Arqueolóxica",
  },
  {
    url: "/images/ilustracion/ilusthcabdab2.webp",
    url_tablet: "/images/ilustracion/tabletilusthcaab2.webp",
    url_mobile: "/images/ilustracion/mobileilusthcaab2.webp",
    caption: "BD Histórica",
    alt: "BD Histórica",
  },
  {
    url: "/images/ilustracion/humorgraf3ab.webp",
    url_tablet: "/images/ilustracion/tablethumorgraf3ab.webp",
    url_mobile: "/images/ilustracion/mobilehumorgraf3ab.webp",
    caption: "Humor gráfico",
    alt: "Humor gráfico",
  },
];

const indicators1 = (index) => (
  <div className="indicator">{slideImages1[index].caption}</div>
);
const indicators2 = (index) => (
  <div className="indicator">{slideImages2[index].caption}</div>
);

const IlustracionPage = () => {
  return (
    <>
      <Header />
      <main>
        <figure id="portadailustracion"></figure>

        <section className="ilustracion-section1">
          <h1>Ilustración</h1>
          <div className="slide-container">
            <Fade indicators={indicators1}>
              {slideImages1.map((fadeImage, index) => (
                <div key={index}>
                  <img
                    className="desktop"
                    style={{ height: "100%" }}
                    src={fadeImage.url}
                    alt={fadeImage.caption}
                  />
                  <img
                    className="tablet"
                    style={{ height: "100%" }}
                    src={fadeImage.url_tablet}
                    alt={fadeImage.caption_tablet}
                  />
                  <img
                    className="mobile"
                    style={{ height: "100%" }}
                    src={fadeImage.url_mobile}
                    alt={fadeImage.caption_mobile}
                  />
                  <h2>{fadeImage.caption}</h2>
                </div>
              ))}
            </Fade>
          </div>
        </section>
        <section className="txtilust">
          <div>
            <p>
              Realiza traballos de ilustración histórica, live painting, banda
              deseñada, humor gráfico, ilustración publicitaria, ilustración
              infantil e xuvenil, ilustración editorial, ilustración de xogos de
              mesa e de manuais de xogos de rol e infográfica. Emprega técnicas
              diversas, especialmente a acuarela, a tinta e as técnicas dixitais
              coa propia intelixencia humana, coa creatividade e pensamento
              abstracto inherentes.
            </p>
          </div>
        </section>
        <section className="ilustracion-section2">
          <div className="slide-container">
            <Fade indicators={indicators2}>
              {slideImages2.map((fadeImage, index) => (
                <div key={index}>
                  <img
                    className="desktop"
                    style={{ height: "100%" }}
                    src={fadeImage.url}
                    alt={fadeImage.caption}
                  />
                  <img
                    className="tablet"
                    style={{ height: "100%" }}
                    src={fadeImage.url_tablet}
                    alt={fadeImage.caption_tablet}
                  />
                  <img
                    className="mobile"
                    style={{ height: "100%" }}
                    src={fadeImage.url_mobile}
                    alt={fadeImage.caption_mobile}
                  />
                </div>
              ))}
            </Fade>
          </div>
        </section>
        <section className="txtilust">
          <div>
            <h2>Curiosidades e rumores...</h2>
            <p class="txtcuriosidades">
              Din as malas linguas que non houbo berce que a retivese de bebé.
              Pois coa ilustración, igual. Despois de moitos intentos de
              especializarse nalgo, decidiu deixarse levar polo factor sorpresa
              de cara a futuros encargos. No cole, ilustraba os seus libros de
              texto e cadernos de apuntamentos, ao seu estilo. Empregaba a banda
              deseñada sempre que tiña ocasión.
            </p>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default IlustracionPage;

export const Head = () => <SEO title="Ilustración" />;
