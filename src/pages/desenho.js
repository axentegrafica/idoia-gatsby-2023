import * as React from "react";
import { Header } from "../components/Header";
import { Footer } from "../components/Footer";
import { Fade } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import "./desenho.css";
import { SEO } from "../components/seo";

const slideImages1 = [
  {
    url: "/images/desenho/escritoriodescorp1.webp",
    url_tablet: "/images/desenho/tabletdescorp1.webp",
    url_mobile: "/images/desenho/mobiledescorp1.webp",
    caption: "Identidade visual",
    alt: "Identidade visual",
  },
  {
    url: "/images/desenho/escritoriodescontdix.webp",
    url_tablet: "/images/desenho/tabletdescontdix.webp",
    url_mobile: "/images/desenho/mobiledescontdix.webp",
    caption: "Contidos dixitais",
    alt: "Contidos dixitais",
  },
  {
    url: "/images/desenho/escritoriodesetiquetado.webp",
    url_tablet: "/images/desenho/tabletdesetiquetado.webp",
    url_mobile: "/images/desenho/mobiledesetiquetado.webp",
    caption: "Etiquetado",
    alt: "Etiquetado",
  },
  {
    url: "/images/desenho/escritoriodescartelismo.webp",
    url_tablet: "/images/desenho/tabletdescartelismo.webp",
    url_mobile: "/images/desenho/mobiledescart.webp",
    caption: "Cartelismo",
    alt: "Cartelismo",
  },
  {
    url: "/images/desenho/escritoriodespub.webp",
    url_tablet: "/images/desenho/tabletdespub.webp",
    url_mobile: "/images/desenho/mobiledespub.webp",
    caption: "Publicidade",
    alt: "Publicidade",
  },
  {
    url: "/images/desenho/escritoriodesinfog.webp",
    url_tablet: "/images/desenho/tabletdesinfog.webp",
    url_mobile: "/images/desenho/mobiledesinfog.webp",
    caption: "Infografía",
    alt: "Infografía",
  },
  {
    url: "/images/desenho/escritoriodesxogm.webp",
    url_tablet: "/images/desenho/tabletdesxogm.webp",
    url_mobile: "/images/desenho/mobiledesxogosm.webp",
    caption: "Xogos de mesa",
    alt: "Xogos de mesa",
  },
];

const slideImages2 = [
  {
    url: "/images/desenho/escritoriodescorp2.webp",
    url_tablet: "/images/desenho/tabletdescorp2.webp",
    url_mobile: "/images/desenho/mobiledescorp2.webp",
    caption: "Identidade corporativa",
    alt: "Identidade corporativa",
  },
  {
    url: "/images/desenho/escritoriodesetiquetado3.webp",
    url_tablet: "/images/desenho/tabletdesetiquetado3.webp",
    url_mobile: "/images/desenho/mobiledesetiquetado3.webp",
    caption: "Etiquetado",
    alt: "Etiquetado",
  },
  {
    url: "/images/desenho/escritoriodesinfog2.webp",
    url_tablet: "/images/desenho/tabletdesinfog2.webp",
    url_mobile: "/images/desenho/mobiledesinfog2.webp",
    caption: "Infográfica",
    alt: "Infográfica",
  },
];

const indicators1 = (index) => (
  <div className="indicator">{slideImages1[index].caption}</div>
);
const indicators2 = (index) => (
  <div className="indicator">{slideImages2[index].caption}</div>
);

const DesenhoPage = () => {
  return (
    <>
      <Header />
      <main>
        <figure id="portadadesenho"></figure>

        <section className="desenho-section1">
          <h1>Deseño gráfico</h1>
          <div className="slide-container">
            <Fade indicators={indicators1}>
              {slideImages1.map((fadeImage, index) => (
                <div key={index}>
                  <img
                    className="desktop"
                    style={{ height: "100%" }}
                    src={fadeImage.url}
                    alt={fadeImage.caption}
                  />
                  <img
                    className="tablet"
                    style={{ height: "100%" }}
                    src={fadeImage.url_tablet}
                    alt={fadeImage.caption_tablet}
                  />
                  <img
                    className="mobile"
                    style={{ height: "100%" }}
                    src={fadeImage.url_mobile}
                    alt={fadeImage.caption_mobile}
                  />
                  <h2>{fadeImage.caption}</h2>
                </div>
              ))}
            </Fade>
          </div>
        </section>

        <section className="txtdes">
          <div className="servicesdes">
            <div className="creatividadedes">
              <img
                className="servizosimgsdes"
                src="/images/desenho/aeaox.gif"
                alt="Aquí e Acolá, O Xusto"
              />
            </div>
          </div>
          <div>
            <p>
              Traballa en proxectos de deseño gráfico de xeito multidisciplinar.
              Especialízase en deseño editorial, deseño de identidade visual
              corporativa, deseño de aplicacións de identidade visual
              corporativa, deseño de contidos dixitais, etiquetado de produtos
              ecolóxicos, con denominación de orixe ou indicación xeográfica
              protexida, etiquetas de viño..., cartelismo, gráfica publicitaria
              impresa e en liña, deseño gráfico aplicado á rotulación e deseño
              de xogos de mesa.
            </p>
            <p>
              Como proxectos destacados dos últimos anos, ten deseñado a
              identidade gráfica do Premio Galego de Educación para o
              Desenvolvemento e Cidadanía Global. Ten deseñado campañas para
              Coruña Viva. Desenvolveu infográfica para catálogos e webs de
              distinta índole: mapas, organizadores gráficos, planos de cidades,
              de edificacións, códigos ideográficos como pictogramas,
              transcricións e interpretacións técnicas para exposicións
              históricas e semellantes.
            </p>
          </div>
        </section>
        <section className="ilustracion-section2">
          <div className="slide-container">
            <Fade indicators={indicators2}>
              {slideImages2.map((fadeImage, index) => (
                <div key={index}>
                  <img
                    className="desktop"
                    style={{ height: "100%" }}
                    src={fadeImage.url}
                    alt={fadeImage.caption}
                  />
                  <img
                    className="tablet"
                    style={{ height: "100%" }}
                    src={fadeImage.url_tablet}
                    alt={fadeImage.caption_tablet}
                  />
                  <img
                    className="mobile"
                    style={{ height: "100%" }}
                    src={fadeImage.url_mobile}
                    alt={fadeImage.caption_mobile}
                  />
                </div>
              ))}
            </Fade>
          </div>
        </section>
        <section className="txtilust">
          <div>
            <h2>Curiosidades e rumores...</h2>
            <p class="txtcuriosidades">
              Comezou a súa carreira na grande urbe catalana do deseño,
              Barcelona, grazas a recomendacións desde a escola Massana. Ten
              traballado en Barcelona e na súa cidade natal, Ourense, para
              axencias de publicidade, editoriais, nunha imprenta especializada
              en grande formato e pola súa conta para múltiples sectores. A súa
              variada experiencia aportoulle versatilidade profesional, desde a
              sinalética de carreteiras ao deseño textil deportivo.
            </p>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default DesenhoPage;

export const Head = () => <SEO title="Deseño Gráfico" />;
