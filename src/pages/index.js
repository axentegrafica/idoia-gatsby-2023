import * as React from "react";
import { Header } from "../components/Header";
import { Footer } from "../components/Footer";
import { Fade } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import "./index.css";
import { SEO } from "../components/seo";

const slideImages1 = [
  {
    url: "/images/ilustracion/ilushcaab.webp",
    url_tablet: "/images/ilustracion/tabletilusthcaab.webp",
    url_mobile: "/images/ilustracion/mobileilusthcaab.webp",
  },
  {
    url: "/images/ilustracion/ilusthcabdab2.webp",
    url_tablet: "/images/ilustracion/tabletilusthcaab2.webp",
    url_mobile: "/images/ilustracion/mobileilusthcaab2.webp",
  },
  {
    url: "/images/ilustracion/ilushca3ab.webp",
    url_tablet: "/images/ilustracion/tabletilusthca3ab.webp",
    url_mobile: "/images/ilustracion/mobileilusthca3ab.webp",
  },
  {
    url: "/images/ilustracion/escritorioilustbdhca2.webp",
    url_tablet: "/images/ilustracion/tabletilustbdhca.webp",
    url_mobile: "/images/ilustracion/mobileilustbdhca2.webp",
  },
  {
    url: "/images/ilustracion/humorgraf3ab.webp",
    url_tablet: "/images/ilustracion/tablethumorgraf3ab.webp",
    url_mobile: "/images/ilustracion/mobilehumorgraf3ab.webp",
  },
  {
    url: "/images/desenho/escritoriodescorp1.webp",
    url_tablet: "/images/desenho/tabletdescorp1.webp",
    url_mobile: "/images/desenho/mobiledescorp1.webp",
  },
  {
    url: "/images/desenho/escritoriodesinfog3.webp",
    url_tablet: "/images/desenho/tabletdesinfog3.webp",
    url_mobile: "/images/desenho/mobiledesinfog3.webp",
  },
  {
    url: "/images/editorial/escritorioeditcat.webp",
    url_tablet: "/images/editorial/tableteditcat.webp",
    url_mobile: "/images/editorial/mobileeditcat.webp",
  },
  {
    url: "/images/editorial/escritorioeditcatxap.webp",
    url_tablet: "/images/editorial/tableteditcatxap.webp",
    url_mobile: "/images/editorial/mobileeditcatxap.webp",
  },
];

const indicators1 = (index) => (
  <div className="indicator">{slideImages1[index].caption}</div>
);

const IndexPage = () => {
  return (
    <>
      <Header />
      <main>
        <figure id="fotoportada"></figure>
        <section className="services">
          <h1>Servizos</h1>
          <ul>
            <li className="creatividade">
              <a href="mailto:info@idoiadeluxan.com">
                <img
                  className="servizosimgs"
                  src="/images/creatividadeg.webp"
                  alt="creatividade"
                />
              </a>
              <h2>Creatividade</h2>
            </li>
            <li className="grafico">
              <a href="desenho" target="_blank">
                <img
                  className="servizosimgs"
                  src="/images/desenhog.webp"
                  alt="deseño gráfico e infografía"
                />
              </a>
              <h2>Deseño gráfico & infografía</h2>
            </li>
            <li className="ilustracion">
              <a href="ilustracion" target="_blank">
                <img
                  className="servizosimgilustracion"
                  src="/images/ilustraciong.webp"
                  alt="ilustración"
                />
              </a>
              <h2>Ilustración</h2>
            </li>
          </ul>
          <section id="targets">
            <div className="destinatarios">
              <div>
                <h3>
                  Para cooperativas, iniciativas e entidades de economía social
                </h3>
              </div>
              <div>
                <h3>
                  Para calquera persoa ou profesional de calquera punto do globo
                </h3>
              </div>
              <div>
                <h3>
                  Para PYMES e profesionais autónomas, imprentas, editoriais...
                </h3>
              </div>
              <div>
                <h3>
                  Para ONGs, asociacións, grupos de participación social,
                  centros...
                </h3>
              </div>
              <div>
                <h3>
                  Para calquera proxecto, empresa, axencia, organismo público...
                </h3>
              </div>
            </div>
          </section>
        </section>
        <section className="txtilust">
          <div>
            <h4>EXPERIENCIA</h4>
            <p>
              Ourense, 1984. Cursou Artes Gráficas na Escola Joso de BD e
              Gráfica Publicitaria na Escola Massana. Traballou para axencias,
              imprentas e como freelance para clientela moi diversa en distintos
              puntos do planeta, desde o 2007.
            </p>
            <p>
              {" "}
              Aportou a súa visión sobre o homenaxeado Chichi Campos, polo Día
              da Ilustración 2024. Foi unha das humoristas gráficas convidadas
              ao 30 aniversario do AVE. Ilustrou láminas sobre a Antiga Grecia
              para os calendarios da Rede Pastwomen 2023 e 2024. É ilustradora
              residente en O Globo para Solidariedade Internacional de Galicia.
              Deseñou o Premio Galego de Educación para o Desenvolvemento e
              Cidadanía Global. Ten participado en numerosas publicacións de
              banda deseñada e ilustración para editoriais, museos,
              universidades, asociacións, ONGs e entidades diversas.
            </p>
          </div>
        </section>
        <section className="ilustracion-section1">
          <div className="slide-container">
            <Fade indicators={indicators1}>
              {slideImages1.map((fadeImage, index) => (
                <div key={index}>
                  <img
                    className="desktop"
                    style={{ height: "100%" }}
                    src={fadeImage.url}
                    alt={fadeImage.caption}
                  />
                  <img
                    className="tablet"
                    style={{ height: "100%" }}
                    src={fadeImage.url_tablet}
                    alt={fadeImage.caption_tablet}
                  />
                  <img
                    className="mobile"
                    style={{ height: "100%" }}
                    src={fadeImage.url_mobile}
                    alt={fadeImage.caption_mobile}
                  />
                </div>
              ))}
            </Fade>
          </div>
        </section>
        <section className="clientes-lista-container">
          <div>
            <p className="cl-container">
              "17 anos son moitos traballos e clientela... Á que está e á que
              non: un pracer e grazas por confiar nos meus servizos."
            </p>
            <div id="animacion-clientes-lista-container"></div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default IndexPage;

export const Head = () => <SEO />;
